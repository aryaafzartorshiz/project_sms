package arya.afzar.torshiz.sms;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;

import java.util.ArrayList;
import java.util.List;

import static androidx.recyclerview.widget.GridLayoutManager.*;

public class MainActivity extends AppCompatActivity {
SpaceNavigationView navigationView;
public static int[] dasteha={R.drawable.mazhabi,R.drawable.monasebati,R.drawable.fun};
private String[] namess;
private ArrayList<item_top> items=new ArrayList<>();
    RecyclerView r_top;
RecyclerTopAdapter recyclerTopAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigationView = findViewById(R.id.space);
        navigationView.initWithSaveInstanceState(savedInstanceState);
        navigationView.addSpaceItem(new SpaceItem("", R.drawable.contact_us));
        navigationView.addSpaceItem(new SpaceItem("", R.drawable.settings));
        navigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_info_black_24dp));
        navigationView.addSpaceItem(new SpaceItem("", R.drawable.fav));
        navigationView.changeCurrentItem(3);

        namess =getResources().getStringArray(R.array.names);
        r_top=findViewById(R.id.rel_top);


        r_top.setLayoutManager(new LinearLayoutManager (this, LinearLayoutManager.HORIZONTAL, false));
        r_top.setHasFixedSize(true);
        r_top.addItemDecoration(new CirclePagerIndicatorDecoration());
        getitem();
        recyclerTopAdapter=new RecyclerTopAdapter(items,this);
        r_top.setAdapter(recyclerTopAdapter);

    }

    private void getitem() {
        int count = 0;
        for (String dname : namess) {

            item_top itemTop = new item_top(dname, count,dasteha[count]);
            items.add(itemTop);
            count+=1;
        }
    }

}

