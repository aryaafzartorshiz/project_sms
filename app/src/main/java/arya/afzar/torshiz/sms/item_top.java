package arya.afzar.torshiz.sms;

public class item_top {
    String name;
    int daste_b;
    int img;

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public item_top(String name, int daste_b, int img) {
        this.name = name;
        this.daste_b = daste_b;
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDaste_b() {
        return daste_b;
    }

    public void setDaste_b(int daste_b) {
        this.daste_b = daste_b;
    }
}
