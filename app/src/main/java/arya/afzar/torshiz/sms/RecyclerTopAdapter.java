package arya.afzar.torshiz.sms;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class RecyclerTopAdapter extends RecyclerView.Adapter<RecyclerTopAdapter.ViewHolder> {
    ArrayList<item_top> lists;
    Context ctx;
    LayoutInflater inflater;

    public RecyclerTopAdapter(ArrayList<item_top> list, Context mctx) {
        this.lists = list;
        ctx = mctx;
        inflater = LayoutInflater.from(ctx);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.recycler_top_item, null, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(lp);
        return new RecyclerTopAdapter.ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final item_top gh = lists.get(position);

        holder.txtmatn.setText(gh.getName() + "...");
        holder.txtid.setText(gh.getDaste_b() + "");
        holder.imageView.setImageResource(gh.getImg());
    }


    @Override
    public int getItemCount() {
        return lists.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtid, txtmatn;
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtid = itemView.findViewById(R.id.txtdaste);
            txtmatn = itemView.findViewById(R.id.txtitem_top);
            imageView = itemView.findViewById(R.id.pic);
        }

    }
}
